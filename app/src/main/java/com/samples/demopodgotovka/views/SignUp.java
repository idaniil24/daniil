package com.samples.demopodgotovka.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.samples.demopodgotovka.R;
import com.samples.demopodgotovka.views.SignIn;

public class SignUp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_sign_up);
    }
    public void moveToLogIn(View view) {
        startActivity(new Intent(getApplicationContext(), SignIn.class));
    }

}