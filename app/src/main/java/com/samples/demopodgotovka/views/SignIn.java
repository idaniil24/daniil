package com.samples.demopodgotovka.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.samples.demopodgotovka.R;

public class SignIn extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        getSupportActionBar().hide();
    }

    public void moveToRegistration(View view) {
        startActivity(new Intent(getApplicationContext(), SignUp.class));
    }


    public void moveToBn(View view) {
        startActivity(new Intent(getApplicationContext(), BotomNavigationActivity.class));
    }
}