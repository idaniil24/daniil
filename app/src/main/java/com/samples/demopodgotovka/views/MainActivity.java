package com.samples.demopodgotovka.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.samples.demopodgotovka.R;
import com.samples.demopodgotovka.views.SignIn;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        ImageView splashImage = findViewById(R.id.splash_icon);
        Animation splahAnimation = AnimationUtils.loadAnimation(this, R.anim.new_anim);
        splashImage.setAnimation(splahAnimation);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), SignIn.class));
            }
        }, 3000);
    }
}