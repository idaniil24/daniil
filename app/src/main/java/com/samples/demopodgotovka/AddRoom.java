package com.samples.demopodgotovka;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.samples.demopodgotovka.views.BotomNavigationActivity;

public class AddRoom extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_add_room);
    }
    public void moveToBn (View view){
        startActivity(new Intent(AddRoom.this, BotomNavigationActivity.class));
    }
}